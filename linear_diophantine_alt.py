import random
random.seed()
import os
import copy
from sympy.parsing.sympy_parser import parse_expr
from sympy.solvers.diophantine import diop_linear
from sympy.simplify import simplify
import re
import string
def substract(a: str, b: str):
    if b:
        return "".join(a.rsplit(b))
    else:
        return a

variable_pattern = re.compile(r"[a-z]+[0-9]*")

class LinearCSYS:
    """

    Работаем с уравнениями вида ax1 + bx2 + cx3 + ... + zxn = m
    х1, х2 ... xn - неизвестные
    a - z - коэффициенты(может быть больше/меньше чем 26)
    Добавить функции загрузки таблицы числовых эквивалентов, шифра

    *вынести загрузку уравнения в отдельную процедуру и из конструктора там тоже повытаскивать

    """
    def __init__(self):
        self.key_size = -1
        self.block_size = -1

        # Valuable data for some functions
        self.data = b''
        self.byte_number_table = {}
        self.number_byte_table = {}
        self.key = []
        self.encrypted = []
        self.decrypted = []

    def set_block_size(self, block_size):
        self.block_size = block_size

    def set_key_size(self,key_size):
        self.key_size = key_size

    def load_data(self,data):
        if len(data) % self.block_size != 0:
            raise Exception(f"Длина данных должна быть кратна {self.block_size}")
        self.data = [data[i:i + self.block_size] for i in range(0, len(data), self.block_size)]

    def load_ciphered_data(self, cipher:str):
        self.encrypted = [int(c.strip()) for c in cipher.split(",")]

    def load_equation(self, equation:str):
        self.equation = equation
        self.variables = re.findall(variable_pattern, self.equation)

    def load_key(self, key):
        self.key = key

    def load_tables(self, byte_int, int_byte):
        """
        :param byte_int:
        :param int_byte:
        :return:
        """
        self.byte_number_table = byte_int
        self.number_byte_table = int_byte

    def generate_tables(self, n):
        if self.data == b'':
            raise Exception(f"Не загружены данные; загрузите данные при помощи load_data и повторите попытку")
        if (len(b"".join(self.data)) // self.block_size) > (2 ** (8 * n)):
            raise Exception("Не хватает байт для представления всех элементов, увеличьте n и повторите попытку")
        used_bytes = set()
        for i in range(len(self.data)):
            while True:
                num = int.from_bytes(os.urandom(n), byteorder='little')
                if num not in used_bytes:
                    used_bytes.add(num)
                    self.byte_number_table[self.data[i]] = num
                    break
        for item in self.byte_number_table:
            self.number_byte_table[self.byte_number_table[item]] = item

    def generate_equation_with_key(self, coeffs_amount):
        """

        Генерирует линейное диофантово уравнение с указанным количеством слагаемых.
        :param coeffs_amount: количество слагаемых
        :return: уравнение в виде строки, список коэффициентов в уравнении и n-1 неизвестное значение
        """
        if 2 * coeffs_amount - 1 > self.key_size:
            raise Exception(f"Недостаточная длина ключа для представления уравнения с количеством коэффициентов {coeffs_amount}")

        # Коэффициенты и неизвестные значения
        coeffs_vals = [1] * (coeffs_amount * 2 - 1)
        bytes_per_coeff = [1] * (coeffs_amount * 2 - 1)
        bytes_left = self.key_size
        while bytes_left - len(bytes_per_coeff) > 0:
            incr = random.randint(0, len(bytes_per_coeff)-1)
            bytes_per_coeff[incr] += 1
            bytes_left -= 1
        for i in range(len(coeffs_vals)):
            coeffs_vals[i] = int.from_bytes(os.urandom(bytes_per_coeff[i]), byteorder='little')
            # fix
            if coeffs_vals[i] == 0:
                coeffs_vals[i] = 1

        # Разделение на коэффициенты уравнения и n-1 неизвестное значение
        coeffs = coeffs_vals[0:coeffs_amount]
        vals = coeffs_vals[coeffs_amount:]

        # Символы переменных
        var_symbols = []
        cntr = 1
        for i in range(coeffs_amount):
            var_symbols.append(string.ascii_lowercase[i % len(string.ascii_lowercase)] + str(cntr))
            cntr = len(var_symbols)//26 + 1

        # Сборка уравнения
        equation = " + ".join([str(coeffs[i]) + "*" + str(var_symbols[i]) for i in range(coeffs_amount)])
        return equation, vals

    def encrypt_data(self):
        if len(self.data) == 0:
            raise Exception("Нечего зашифровывать")
        if self.key == -1:
            raise Exception("Загрузите ключ и повторите попытку")
        if len(self.byte_number_table) == 0 or len(self.number_byte_table) == 0:
            raise Exception("Создайте таблицы соответствия при помощи функции generate_tables() и повторите попытку")


        # Производим вычисления, заменяя последнюю переменную на значение
        # шифруемого текста из таблицы численных эквивалентов
        self.encrypted = [0] * len(self.data)
        for i in range(len(self.data)):
            # Выбираем очередную переменную
            # Осуществляем замены остальных переменных на заданные коэффициенты
            variables = copy.deepcopy(self.variables)
            equation = copy.deepcopy(self.equation)
            current_variable = variables[i%len(variables)]
            variables.remove(current_variable)
            for variable, key in zip(variables, self.key):
                equation = equation.replace(variable, str(key))
            self.encrypted[i] = int(
                parse_expr(
                    equation.replace(
                        current_variable, str(
                            self.byte_number_table[self.data[i]]
                        )
                    )
                )
            )

    def decrypt_cipher(self):
        if len(self.encrypted) == 0:
            raise Exception("Шифротекста нет, используйте load_ciphered_data() и повторите попытку")
        if self.key == -1:
            raise Exception("Загрузите ключ и повторите попытку")
        if len(self.byte_number_table) == 0 or len(self.number_byte_table) == 0:
            raise Exception("Generate tables with self.generate_tables() function and try again")



        # Добавляем с обратным знаком значение зашифрованного текста
        self.decrypted = [b''] * len(self.encrypted)
        for i in range(len(self.encrypted)):
            variables = copy.deepcopy(self.variables)
            equation = copy.deepcopy(self.equation)
            current_variable = variables[i % len(variables)]
            variables.remove(current_variable)
            for variable, key in zip(variables, self.key):
                equation = equation.replace(variable, str(key))
            self.decrypted[i] = self.number_byte_table[
                int(diop_linear(
                    parse_expr(
                        equation + " - " + str(self.encrypted[i])
                    )
                )[0])]



if __name__ == "__main__":
    text = "Шифротекст".encode("utf-8")
    block_size = 2
    cipher = LinearCSYS()
    cipher.set_key_size(16)
    cipher.set_block_size(block_size)
    cipher.load_data(text)
    cipher.generate_tables(2)
    equ, key = cipher.generate_equation_with_key(6)
    print(equ, key)
    print(cipher.byte_number_table)
    cipher.load_equation(equ)
    cipher.load_key(key)
    cipher.encrypt_data()
    print(cipher.encrypted)
    cipher.decrypt_cipher()
    print(b"".join(cipher.decrypted))
    assert text == b"".join(cipher.decrypted)
