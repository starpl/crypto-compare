from wolfcrypt.ciphers import Aes, MODE_CBC
from linear_diophantine_alt import LinearCSYS
import os, time
from ctypescrypto.cipher import CipherType, Cipher
from ctypescrypto.engine import Engine, set_default
import blowfish
import csv

def print_cipher_info(cipher):
    print(f"Cipher name: {cipher.algo()} \n"
          f"Key length: {cipher.key_length()} \n"
          f"Block size: {cipher.block_size()} \n"
          f"IV length: {cipher.iv_length()} \n"
          f"Mode: {cipher.mode()} \n"
          f"Flags: {cipher.flags()} \n"
          f"OID: {cipher.oid()}")


def write_to_csv(name:str, data:list):
      with open(name,'w') as csv_file:
          field_names = data[0].keys()
          writer = csv.DictWriter(csv_file, fieldnames=field_names)
          writer.writeheader()
          for element in data:
                writer.writerow(element)


def diophantine_test(text, key_len, block_size):
    '''diophantine'''

    # print('\nDiophantine:\n______________________________________\n')
    cipher = LinearCSYS()
    cipher.set_key_size(key_len)
    cipher.set_block_size(block_size)
    cipher.load_data(text)
    # Prefix size decreases when n increases
    cipher.generate_tables(8)
    equation, key = cipher.generate_equation_with_key(16)
    cipher.load_key(key)
    cipher.load_equation(equation)

    # Encryption
    start_time = time.time()
    cipher.encrypt_data()
    duration_enc = time.time() - start_time
    #cipher.compress_cipher()
    bits = 0
    for i in range(len(cipher.encrypted)):
        bits += cipher.encrypted[i].bit_length()
    throughput_enc = ((bits + 7) // 8) / duration_enc
    # print(f"Encryption time: {format(duration_enc, '.30f')}\n"
    #       f"Length of ciphertext: {(bits + 7)// 8}\n"
    #       f"Throughput: {throughput_enc}\n")

    # Decryption
    # cipher.decompress_cipher()

    start_time = time.time()
    cipher.decrypt_cipher()
    duration_dec = time.time() - start_time
    throughput_dec = len(b"".join(cipher.decrypted)) / duration_dec
    # print(f"Decryption time: {format(duration_dec, '.30f')}\n"
    #       f"Length of decrypted data: {len(decrypted)}\n"
    #       f"Throughput: {throughput_dec}\n"
    #       f"______________________________________\n")

    assert text == b"".join(cipher.decrypted)
    diophantine_data.append({'key_size': str(cipher.key_size),
                             'block_size': str(cipher.block_size),
                             'input_size': str(len(text)),
                             'output_size': str((bits + 7) // 8),
                             'enc_time': format(duration_enc, digits),
                             'dec_time': format(duration_dec, digits),
                             'throughput_enc': format(throughput_enc, digits),
                             'throughput_dec': format(throughput_dec, digits)})


def aes_openssl_test(text):
    '''Aes-128(OpenSSL)'''
    # Rewrite for OpenSSL's Blowfish
    # print("\nAES-128(OpenSSL):\n______________________________________\n")

    aes_ciphertype = CipherType('aes-256-ecb')
    # print_cipher_info(aes_ciphertype)

    # Setting encryptor / decryptor
    aes_32_enc = Cipher(aes_ciphertype, key_32, iv_0)
    aes_32_dec = Cipher(aes_ciphertype, key_32, iv_0, encrypt=False)

    # Encrypt
    start_time = time.time()
    aes_32_ciphered_data = aes_32_enc.update(text) + aes_32_enc.finish()
    duration_enc = time.time() - start_time
    throughput_enc = len(aes_32_ciphered_data) / duration_enc
    # print(f"Encryption time: {format(duration_enc, '.30f')}\n"
    #       f"Length of ciphertext: {len(aes_16_ciphered_data)}\n"
    #       f"Throughput: {throughput_enc}\n")

    # Decrypt
    start_time = time.time()
    aes_32_deciphered_data = aes_32_dec.update(aes_32_ciphered_data) + aes_32_dec.finish()
    duration_dec = time.time() - start_time
    throughput_dec = len(aes_32_deciphered_data) / duration_dec
    # print(f"Encryption time: {format(duration_dec, '.30f')}\n"
    #       f"Length of ciphertext: {len(aes_16_deciphered_data)}\n"
    #       f"Throughput: {throughput_dec}\n")

    aes_openssl_data.append({'key_size': str(aes_ciphertype.key_length()),
                             'block_size': str(aes_ciphertype.block_size()),
                             'input_size': str(len(text)),
                             'output_size': str(len(aes_32_ciphered_data)),
                             'enc_time': format(duration_enc, digits),
                             'dec_time': format(duration_dec, digits),
                             'throughput_enc': format(throughput_enc, digits),
                             'throughput_dec': format(throughput_dec, digits)})


def blowfish_test(text):
    '''Blowfish'''
    # print("\nBlowfish:\n______________________________________\n")

    blowfish_ciphertype = CipherType('bf-ecb')
    # print_cipher_info(blowfish_ciphertype)

    # Setting encryptor / decryptor
    blowfish_enc = Cipher(blowfish_ciphertype, key_32, iv_0)
    blowfish_dec = Cipher(blowfish_ciphertype, key_32, iv_0, encrypt=False)

    # Encrypt
    start_time = time.time()
    blowfish_ciphered_data = blowfish_enc.update(text) + blowfish_enc.finish()
    duration_enc = time.time() - start_time
    throughput_enc = len(blowfish_ciphered_data) / duration_enc
    # print(f"Encryption time: {format(duration_enc, '.30f')}\n"
    #       f"Length of ciphertext: {len(blowfish_ciphered_data)}\n"
    #       f"Throughput: {throughput_enc}\n")

    # Decrypt
    start_time = time.time()
    blowfish_deciphered_data = blowfish_dec.update(blowfish_ciphered_data) + blowfish_dec.finish()
    duration_dec = time.time() - start_time
    throughput_dec = len(blowfish_deciphered_data) / duration_dec
    # print(f"Encryption time: {format(duration_dec, '.30f')}\n"
    #       f"Length of ciphertext: {len(blowfish_deciphered_data)}\n"
    #       f"Throughput: {throughput_dec}\n")

    blowfish_data.append({'key_size': str(blowfish_ciphertype.key_length()),
                          'block_size': str(blowfish_ciphertype.block_size()),
                          'input_size': str(len(text)),
                          'output_size': str(len(blowfish_ciphered_data)),
                          'enc_time': format(duration_enc, digits),
                          'dec_time': format(duration_dec, digits),
                          'throughput_enc': format(throughput_enc, digits),
                          'throughput_dec': format(throughput_dec, digits)})


def grasshopper_test(text):
    '''Grasshopper'''

    # Set OpenSSL engine for GOST algorithms
    set_default('gost', 0xFFFF)

    # print("Grasshopper:\n______________________________________\n")
    grasshopper_ciphertype = CipherType('grasshopper-ecb')
    # print_cipher_info(grasshopper_ciphertype)

    # Setting encryptor / decryptor
    grasshopper_enc = Cipher(grasshopper_ciphertype, grasshopper_key, grasshopper_iv)
    grasshopper_dec = Cipher(grasshopper_ciphertype, grasshopper_key, grasshopper_iv, encrypt=False)

    # Encrypt
    start_time = time.time()
    grasshopper_ciphered_data = grasshopper_enc.update(text) + grasshopper_enc.finish()
    duration_enc = time.time() - start_time
    throughput_enc = len(grasshopper_ciphered_data) / duration_enc
    # print(f"Encryption time: {format(duration_enc, '.30f')}\n"
    #       f"Length of ciphertext: {len(grasshopper_ciphered_data)}\n"
    #       f"Throughput: {throughput_enc}\n")

    # Decrypt
    start_time = time.time()
    grasshopper_deciphered_data = grasshopper_dec.update(grasshopper_ciphered_data) + grasshopper_dec.finish()
    duration_dec = time.time() - start_time
    throughput_dec = len(grasshopper_deciphered_data) / duration_dec
    # print(f"Decryption time {format(duration_dec, '.30f')}\n"
    #       f"Length of decrypted data: {len(grasshopper_deciphered_data)}\n"
    #       f"Throughput: {throughput_dec}\n"
    #       f"______________________________________")

    grasshopper_data.append({'key_size': str(grasshopper_ciphertype.key_length()),
                             'block_size': str(grasshopper_ciphertype.block_size()),
                             'input_size': str(len(text)),
                             'output_size': str(len(grasshopper_ciphered_data)),
                             'enc_time': format(duration_enc, digits),
                             'dec_time': format(duration_dec, digits),
                             'throughput_enc': format(throughput_enc, digits),
                             'throughput_dec': format(throughput_dec, digits)})


def aes_wolfcrypt_test(text):
    '''AES-Wolfcrypt'''
    cipher = Aes(key_32, MODE_CBC, iv_16)
    # print('AES(Wolfcrypt-py):\n______________________________________\n')

    # Encrypt
    start_time = time.time()
    encrypted = cipher.encrypt(text)
    duration_enc = time.time() - start_time
    throughput_enc = len(encrypted) / duration_enc
    # print(f"Encryption time: {format(duration_enc, '.30f')}\n"
    #       f"Length of ciphertext: {len(encrypted)}\n"
    #       f"Throughput: {throughput_enc}\n")

    # Decryption
    start_time = time.time()
    decrypted = cipher.decrypt(encrypted)
    duration_dec = time.time() - start_time
    throughput_dec = len(decrypted) / duration_dec
    # print(f"Decryption time: {format(duration_dec, '.30f')}\n"
    #       f"Length of decrypted data: {len(decrypted)}\n"
    #       f"Throughput: {throughput_dec}\n"
    #       f"______________________________________\n")

    aes_wolf_data.append({'key_size': str(32),
                          'block_size': str(16),
                          'input_size': str(len(text)),
                          'output_size': str(len(encrypted)),
                          'enc_time': format(duration_enc, digits),
                          'dec_time': format(duration_dec, digits),
                          'throughput_enc': format(throughput_enc, digits),
                          'throughput_dec': format(throughput_dec, digits)})


if __name__ == "__main__":
    digits = '.10f'

    '''Text'''
    data_array = [
        os.urandom(2048),
        os.urandom(1024 * 40),
        os.urandom(1024 * 60),
        os.urandom(1024 * 100),
        os.urandom(1024 * 200),
        os.urandom(1024 * 500),
        os.urandom(1024 * 1024),
        os.urandom(1024 * 2048)
    ]

    '''Key Generation'''
    # Key and IV, represented as bytes
    key_32 = os.urandom(32)
    iv_16 = os.urandom(16)
    iv_0 = os.urandom(0)
    grasshopper_key = os.urandom(32)
    grasshopper_iv = os.urandom(0)

    '''Data to collect'''
    diophantine_data = []
    aes_wolf_data = []
    aes_openssl_data = []
    blowfish_data = []
    grasshopper_data = []

    diophantine_block_size = 16

    '''Tests'''
    for text in data_array:
        for i in range(10):
            # aes_openssl_test(text)
            # aes_wolfcrypt_test(text)
            # blowfish_test(text)
            # grasshopper_test(text)
            diophantine_test(text, 32, diophantine_block_size)
        print("completed iteration")

    write_to_csv('diophant.csv', diophantine_data)
    # write_to_csv('aes_wolf.csv', aes_wolf_data)
    # write_to_csv('aes_openssl.csv', aes_openssl_data)
    # write_to_csv('blowfish.csv', blowfish_data)
    # write_to_csv('grasshopper.csv', grasshopper_data)




